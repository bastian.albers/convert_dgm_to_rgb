# convert_dgm_to_rgb
Simple conversion from input greyscale digital elevation model with float32 pixel vlaues to RGB colored model with uint8 pixel values

## Installation
The script needs [rasterio](https://rasterio.readthedocs.io/en/latest/installation.html) and numpy (python standard) installed.

## Use

For simple [RGB conversion](https://gis.stackexchange.com/a/272805/10718) start the script with:

```bash
$ python convert_dgm_to_rgb.py -i INPUT.tif
```
For [terrarium like conversion](https://github.com/tilezen/joerd/blob/master/docs/formats.md#terrarium) start the script with

```bash
$ python convert_dgm_to_rgb.py -i INPUT.tif -t terrarium
```

## Sources
- RGB: https://gis.stackexchange.com/a/272805/10718
- Terrarium: https://github.com/tilezen/joerd/blob/master/docs/formats.md#terrarium
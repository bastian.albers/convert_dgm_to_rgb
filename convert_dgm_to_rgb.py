import os
import logging
import time
import sys
import getopt
import numpy as np
import rasterio

def main(argv):
    logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=logging.INFO,
        datefmt='%Y-%m-%d %H:%M:%S')

    HELP_STRING = 'convert_dgm_to_rgb.py -i <*.tif file> [optional: -t <rgb (standrad), terrarium>]'
    LIST_OF_PROCESS_TYPES = ['rgb' , 'terrarium']
    inputfile = ''
    processtype = LIST_OF_PROCESS_TYPES[0]

    try:
        opts, args = getopt.getopt(argv, "hi:t:", ["ifile=", "type="])
    except getopt.GetoptError:
        print(HELP_STRING)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-help':
            print(HELP_STRING)
            sys.exit()
        elif opt in ("-i", "--ifile"):
            inputfile = arg
        elif opt in ("-t", "--type"):
            if arg not in LIST_OF_PROCESS_TYPES:
                logging.error("Process type must be {}!".format(' or '.join(map(str, LIST_OF_PROCESS_TYPES))))
                sys.exit(2)
            processtype = arg

    logging.info("Starting conversion.")
    start = time.time()

    if inputFileIsOk(inputfile):
        processInputFile(inputfile, processtype)

    end = time.time()
    logging.info("Finished conversion in " + str(int(end - start)) + " seconds.")

def inputFileIsOk(inputfile):
    if inputfile == '':
        logging.error("No input file given! Use -help")
        sys.exit(2)
    if os.path.isfile(inputfile):
        fileName = os.path.splitext(inputfile)
        if fileName[1] == '.tif':
            return True
    logging.error("Inputfile is no .tif file or valid path to .tif file")
    sys.exit(2)
    

def processInputFile(inputfile, processtype):
    with rasterio.open(inputfile) as src:
        dem = src.read(1)

    r = np.zeros(dem.shape)
    g = np.zeros(dem.shape)
    b = np.zeros(dem.shape)

    if processtype == 'rgb':
        # math from https://gis.stackexchange.com/a/272805/10718
        r += np.floor_divide((100000 + dem * 10), 65536)
        g += np.floor_divide((100000 + dem * 10), 256) - r * 256
        b += np.floor(100000 + dem * 10) - r * 65536 - g * 256
    elif processtype == 'terrarium':
        # math from https://github.com/tilezen/joerd/blob/master/docs/formats.md#terrarium
        v = dem + 32768
        r = np.floor(v/256)
        g = np.floor(v % 256)
        b = np.floor((v - np.floor(v)) * 256)

    meta = src.meta
    meta['dtype'] = rasterio.uint8
    meta['nodata'] = 0
    meta['count'] = 3

    dirname = os.path.dirname(inputfile)
    basename = os.path.basename(inputfile).split('.')[0]

    output = os.path.join(dirname, basename + "_" + processtype + ".tif")

    with rasterio.open(output, 'w', **meta) as dst:
        dst.write_band(1, r.astype(rasterio.uint8))
        dst.write_band(2, g.astype(rasterio.uint8))
        dst.write_band(3, b.astype(rasterio.uint8))

if __name__ == "__main__":
   main(sys.argv[1:])